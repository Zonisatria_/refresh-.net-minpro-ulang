USE [master]
GO
/****** Object:  Database [refresh_db]    Script Date: 7/15/2020 4:21:32 PM ******/
CREATE DATABASE [refresh_db]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'refresh_db', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL12.MSSQLSERVER\MSSQL\DATA\refresh_db.mdf' , SIZE = 3264KB , MAXSIZE = UNLIMITED, FILEGROWTH = 1024KB )
 LOG ON 
( NAME = N'refresh_db_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL12.MSSQLSERVER\MSSQL\DATA\refresh_db_log.ldf' , SIZE = 560KB , MAXSIZE = 2048GB , FILEGROWTH = 10%)
GO
ALTER DATABASE [refresh_db] SET COMPATIBILITY_LEVEL = 120
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [refresh_db].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [refresh_db] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [refresh_db] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [refresh_db] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [refresh_db] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [refresh_db] SET ARITHABORT OFF 
GO
ALTER DATABASE [refresh_db] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [refresh_db] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [refresh_db] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [refresh_db] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [refresh_db] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [refresh_db] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [refresh_db] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [refresh_db] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [refresh_db] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [refresh_db] SET  ENABLE_BROKER 
GO
ALTER DATABASE [refresh_db] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [refresh_db] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [refresh_db] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [refresh_db] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [refresh_db] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [refresh_db] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [refresh_db] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [refresh_db] SET RECOVERY FULL 
GO
ALTER DATABASE [refresh_db] SET  MULTI_USER 
GO
ALTER DATABASE [refresh_db] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [refresh_db] SET DB_CHAINING OFF 
GO
ALTER DATABASE [refresh_db] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [refresh_db] SET TARGET_RECOVERY_TIME = 0 SECONDS 
GO
ALTER DATABASE [refresh_db] SET DELAYED_DURABILITY = DISABLED 
GO
EXEC sys.sp_db_vardecimal_storage_format N'refresh_db', N'ON'
GO
USE [refresh_db]
GO
/****** Object:  Table [dbo].[formulir_covid]    Script Date: 7/15/2020 4:21:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[formulir_covid](
	[is_delete] [bit] NOT NULL,
	[e_mail] [varchar](30) NOT NULL,
	[name] [varchar](50) NOT NULL,
	[npm_nis] [varchar](10) NOT NULL,
	[age] [bigint] NOT NULL,
	[gender] [bit] NOT NULL,
	[telp] [varchar](12) NOT NULL,
	[address] [varchar](100) NOT NULL,
	[address_asal] [varchar](100) NOT NULL,
	[domisili] [varchar](50) NOT NULL,
	[id] [bigint] IDENTITY(1,1) NOT NULL,
 CONSTRAINT [PK_formulir_covid_1] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[formulir_covid] ON 

INSERT [dbo].[formulir_covid] ([is_delete], [e_mail], [name], [npm_nis], [age], [gender], [telp], [address], [address_asal], [domisili], [id]) VALUES (0, N'zoni@gmail.co.id', N'Zoni Satria Dodi', N'123456', 20, 1, N'082280602742', N'Salemba', N'Sungai Penuh', N'Salemba', 1)
INSERT [dbo].[formulir_covid] ([is_delete], [e_mail], [name], [npm_nis], [age], [gender], [telp], [address], [address_asal], [domisili], [id]) VALUES (1, N'asdasd', N'asdsad', N'123123', 12, 0, N'21312312321', N'sadfasd', N'asdasd', N'asdsaad', 6)
INSERT [dbo].[formulir_covid] ([is_delete], [e_mail], [name], [npm_nis], [age], [gender], [telp], [address], [address_asal], [domisili], [id]) VALUES (0, N'haries@gmail.com', N'Haries Marhotang', N'123423', 23, 0, N'082276543826', N'Tanggerang', N'Jakarta', N'Jakarta', 7)
INSERT [dbo].[formulir_covid] ([is_delete], [e_mail], [name], [npm_nis], [age], [gender], [telp], [address], [address_asal], [domisili], [id]) VALUES (1, N'coba@gmail.com', N'Coba ugygyugyu', N'1231234', 22, 1, N'081276543787', N'Papua', N'Palembang', N'Palembang', 8)
INSERT [dbo].[formulir_covid] ([is_delete], [e_mail], [name], [npm_nis], [age], [gender], [telp], [address], [address_asal], [domisili], [id]) VALUES (0, N'coba@gmail.com', N'Coba Sudahlah', N'1231234', 22, 1, N'081276543787', N'Papua', N'Palembang', N'Palembang', 9)
INSERT [dbo].[formulir_covid] ([is_delete], [e_mail], [name], [npm_nis], [age], [gender], [telp], [address], [address_asal], [domisili], [id]) VALUES (0, N'haries@gmail.com', N'Haries Marhotang', N'123423', 25, 1, N'082276543826', N'Tanggerang', N'Jakarta', N'Jakarta', 10)
INSERT [dbo].[formulir_covid] ([is_delete], [e_mail], [name], [npm_nis], [age], [gender], [telp], [address], [address_asal], [domisili], [id]) VALUES (0, N'haries@gmail.com', N'Coba ', N'123423', 23, 1, N'082276543826', N'Tanggerang', N'Kota Sungai Penuh', N'Jakarta', 11)
SET IDENTITY_INSERT [dbo].[formulir_covid] OFF
USE [master]
GO
ALTER DATABASE [refresh_db] SET  READ_WRITE 
GO
