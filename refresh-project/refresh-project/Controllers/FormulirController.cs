﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using refresh_project.Models;
using refresh_project.Models.View;
using System.Text.RegularExpressions;


namespace refresh_project.Controllers
{
    public class FormulirController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult List()
        {
            return PartialView(FormulirModelAccess.GetListAll());
        }

        public ActionResult Create()
        {
            return PartialView();
        }
        [HttpPost]
        public ActionResult Create(FormulirModelView paramModel)
        {
            try
            {
                paramModel.is_delete = false;

                FormulirModelAccess.Message = string.Empty;
                if (null == paramModel.name|| null == paramModel.e_mail|| null == paramModel.npm_nis|| null == paramModel.age|| null == paramModel.gender|| null == paramModel.telp|| null == paramModel.address|| null == paramModel.address_asal|| null == paramModel.domisili)
                {
                    FormulirModelAccess.Message += "Anda belum memasukan semua data. Silahkan ulang kembali";
                }
                if (string.IsNullOrEmpty(FormulirModelAccess.Message))
                {
                    return Json(new
                    {
                        success = FormulirModelAccess.Insert(paramModel),
                        message = FormulirModelAccess.Message
                    }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(new { success = false, message = FormulirModelAccess.Message },
                    JsonRequestBehavior.AllowGet);
                    //return View();
                }
            }
            catch (Exception hasError)
            {
                return Json(new { success = false, message = hasError.Message },
                    JsonRequestBehavior.AllowGet);
            }
        }




        public ActionResult Detail(Int32 paramId)
        {
            return PartialView(FormulirModelAccess.GetDetailById(paramId));
        }



        public ActionResult Edit(Int32 paramId)
        {
            return PartialView(FormulirModelAccess.GetDetailById(paramId));
        }

        [HttpPost]
        public ActionResult Edit(FormulirModelView paramModel)
        {
            try
            {
                FormulirModelAccess.Message = string.Empty;

                if (null == paramModel.name || null == paramModel.e_mail || null == paramModel.npm_nis || 0 == paramModel.age || null == paramModel.telp || null == paramModel.address || null == paramModel.address_asal || null == paramModel.domisili)
                {
                    FormulirModelAccess.Message += "Anda belum memasukan semua data. Silahkan ulang kembali";
                }
                if (string.IsNullOrEmpty(FormulirModelAccess.Message))
                {
                    return Json(new
                    {
                        success = FormulirModelAccess.Update(paramModel),
                        message = FormulirModelAccess.Message
                    }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(new { success = false, message = FormulirModelAccess.Message },
                    JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception hasError)
            {
                return Json(new
                {
                    success = false,
                    message = hasError.Message
                },
                JsonRequestBehavior.AllowGet);
            }
        }


        public ActionResult Delete(Int32 Id)
        {
            return PartialView(FormulirModelAccess.GetDetailById(Id));
        }

        [HttpPost]
        public ActionResult Delete(FormulirModelView paramModel)
        {
            try
            {
                return Json(new
                {
                    success = FormulirModelAccess.Delete(paramModel),
                    message = FormulirModelAccess.Message
                }, JsonRequestBehavior.AllowGet);

            }
            catch (Exception hasError)
            {
                return Json(new
                {
                    success = false,
                    message = hasError.Message
                }, JsonRequestBehavior.AllowGet);
            }
        }

    }
}