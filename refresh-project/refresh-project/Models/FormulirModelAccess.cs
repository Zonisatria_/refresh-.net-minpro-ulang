﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using refresh_project.Models.View;

namespace refresh_project.Models
{
    public class FormulirModelAccess
    {
        public static string Message;
        public static List<FormulirModelView> GetListAll()
        {
            List<FormulirModelView> listData = new List<FormulirModelView>();
            using (var db = new refresh_dbEntities())
            {
                listData = (from a in db.formulir_covid
                            where a.is_delete == false
                            select new FormulirModelView
                            {
                                id = a.id,
                                is_delete = a.is_delete,
                                name = a.name,
                                e_mail = a.e_mail,
                                npm_nis = a.npm_nis,
                                age = a.age,
                                gender = a.gender,
                                telp = a.telp,
                                address = a.address,
                                address_asal = a.address_asal,
                                domisili = a.domisili
                            }).ToList();
            }
            return listData;
        }

        public static FormulirModelView GetDetailById(int Id)
        {
            FormulirModelView result = new FormulirModelView();
            using (var db = new refresh_dbEntities())
            {
                result = (from a in db.formulir_covid
                          where a.id == Id
                          select new FormulirModelView
                          {
                              id = a.id,
                              is_delete = a.is_delete,
                              name = a.name,
                              e_mail = a.e_mail,
                              npm_nis = a.npm_nis,
                              age = a.age,
                              gender = a.gender,
                              telp = a.telp,
                              address = a.address,
                              address_asal = a.address_asal,
                              domisili = a.domisili
                          }).FirstOrDefault();
            }
            return result;
        }




        public static bool Insert(FormulirModelView paramModel)
        {
            bool result = true;
            try
            {
                using (var db = new refresh_dbEntities())
                {
                    formulir_covid a = new formulir_covid();
                    a.is_delete = paramModel.is_delete;
                    a.name = paramModel.name;
                    a.e_mail = paramModel.e_mail;
                    a.npm_nis = paramModel.npm_nis;
                    a.age = paramModel.age;
                    a.gender = paramModel.gender;
                    a.telp = paramModel.telp;
                    a.address = paramModel.address;
                    a.address_asal = paramModel.address_asal;
                    a.domisili = paramModel.domisili;

                    db.formulir_covid.Add(a);
                    db.SaveChanges();
                }
            }
            catch (Exception hasError)
            {
                result = false;
                if (hasError.Message.ToLower().Contains("inner exception"))
                {
                    Message = hasError.InnerException.InnerException.Message;
                }
                else
                {
                    Message = hasError.Message;
                }
            }
            return result;
        }


        public static bool Update(FormulirModelView paramModel)
        {
            bool result = true;

            try
            {
                using (var db = new refresh_dbEntities())
                {
                    formulir_covid a = db.formulir_covid.Where(
                        o => o.id == paramModel.id).FirstOrDefault();

                    if (a != null)
                    {
                        a.name = paramModel.name;
                        a.e_mail = paramModel.e_mail;
                        a.npm_nis = paramModel.npm_nis;
                        a.age = paramModel.age;
                        a.gender = paramModel.gender;
                        a.telp = paramModel.telp;
                        a.address = paramModel.address;
                        a.address_asal = paramModel.address_asal;
                        a.domisili = paramModel.domisili;
                        db.SaveChanges();
                    }
                    else
                    {
                        result = false;
                        Message = "Data Tidak Ditemukan";
                    }
                }
            }

            catch (Exception hasError)
            {
                result = false;
                if (hasError.Message.ToLower().Contains("inner exception"))
                {
                    Message = hasError.InnerException.InnerException.Message;
                }
                else
                {
                    Message = hasError.Message;
                }
            }
            return result;
        }



        public static bool Delete(FormulirModelView paramModel)
        {
            bool result = true;

            try
            {
                using (var db = new refresh_dbEntities())
                {
                    formulir_covid a = db.formulir_covid.Where(o => o.id == paramModel.id).FirstOrDefault();
                    if (a != null)
                    {
                        a.is_delete = true;

                        db.SaveChanges();
                    }
                    else
                    {
                        result = false;
                    }
                }
            }
            catch (Exception hasError)
            {
                result = false;
                if (hasError.Message.ToLower().Contains("inner exception"))
                {
                    Message = hasError.InnerException.InnerException.Message;
                }
                else
                {
                    Message = hasError.Message;
                }
            }
            return result;
        }









    }
}