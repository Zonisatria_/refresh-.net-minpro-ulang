﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace refresh_project.Models.View
{
    public class FormulirModelView
    {
        public long id { get; set; }
        public bool is_delete { get; set; }
        [Required(ErrorMessage = "Data harus diisi !")]
        public string e_mail { get; set; }
        [Required(ErrorMessage = "Data harus diisi !")]
        public string name { get; set; }
        [Required(ErrorMessage = "Data harus diisi !")]
        public string npm_nis { get; set; }
        [Required(ErrorMessage = "Data harus diisi !")]
        public long age { get; set; }
        [Required(ErrorMessage = "Data harus diisi !")]
        public bool gender { get; set; }
        [Required(ErrorMessage = "Data harus diisi !")]
        public string telp { get; set; }
        [Required(ErrorMessage = "Data harus diisi !")]
        public string address { get; set; }
        [Required(ErrorMessage = "Data harus diisi !")]
        public string address_asal { get; set; }
        [Required(ErrorMessage = "Data harus diisi !")]
        public string domisili { get; set; }
    }
}